﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    internal class DictionaryItem
    {
        public string UkrainianWord { get; set; }
        public string RussianTranslation { get; set; }
        public string EnglishTranslation { get; set; }
    }
}
