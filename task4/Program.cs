﻿namespace task4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            CustomDictionary dictionary = new CustomDictionary();

            dictionary.AddItem("Привіт", "Привет", "Hello");
            dictionary.AddItem("Дякую", "Спасибо", "Thank you");
            dictionary.AddItem("Доброї ночі", "Спокойной ночи", "Good night");

            string ukrainianWord = "Привіт";
            string russianTranslation = dictionary.GetRussianTranslation(ukrainianWord);
            Console.WriteLine($"Російський переклад слова '{ukrainianWord}': {russianTranslation}");

            string ukrainianWord2 = "Дякую";
            string englishTranslation = dictionary.GetEnglishTranslation(ukrainianWord2);
            Console.WriteLine($"Англійський переклад слова '{ukrainianWord2}': {englishTranslation}");
        }
    }
}