﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    internal class CustomDictionary
    {
        private List<DictionaryItem> items;

        public CustomDictionary()
        {
            items = new List<DictionaryItem>();
        }

        public void AddItem(string ukrainianWord, string russianTranslation, string englishTranslation)
        {
            DictionaryItem item = new DictionaryItem
            {
                UkrainianWord = ukrainianWord,
                RussianTranslation = russianTranslation,
                EnglishTranslation = englishTranslation
            };

            items.Add(item);
        }

        public string GetRussianTranslation(string ukrainianWord)
        {
            foreach (var item in items)
            {
                if (item.UkrainianWord == ukrainianWord)
                {
                    return item.RussianTranslation;
                }
            }

            return null;
        }

        public string GetEnglishTranslation(string ukrainianWord)
        {
            foreach (var item in items)
            {
                if (item.UkrainianWord == ukrainianWord)
                {
                    return item.EnglishTranslation;
                }
            }

            return null;
        }
    }
}
