﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    internal class Calendar 
    {
        private List<string> Month = new List<string>()
        { "January", "February", "March", "April", "May", "June", "July",
        "August", "September", "October", "November", "December" };

        private List<int> Order = new List<int>()
        { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

        private List<int> Days = new List<int>()
        { DateTime.DaysInMonth(DateTime.Now.Year, 1), DateTime.DaysInMonth(DateTime.Now.Year, 2),
        DateTime.DaysInMonth(DateTime.Now.Year, 3), DateTime.DaysInMonth(DateTime.Now.Year, 4),
        DateTime.DaysInMonth(DateTime.Now.Year, 5), DateTime.DaysInMonth(DateTime.Now.Year, 6),
        DateTime.DaysInMonth(DateTime.Now.Year, 7), DateTime.DaysInMonth(DateTime.Now.Year, 8),
        DateTime.DaysInMonth(DateTime.Now.Year, 9), DateTime.DaysInMonth(DateTime.Now.Year, 10),
        DateTime.DaysInMonth(DateTime.Now.Year, 11), DateTime.DaysInMonth(DateTime.Now.Year, 12) };

        public string this[int index]
        {
            get { return Month[index]; }
        }

        public List<string> MonthWithDays(int day)
        {
            List<string> FoundMonths = new List<string>();
            for(int i = 0; i < 12; i++)
            {
                if (Days[i] == day)
                {
                    FoundMonths.Add(Month[i]);
                }
            }
            return FoundMonths;
        }
    }
}
