﻿namespace task1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Calendar calendar = new Calendar();

            for(int i = 0; i < 12; i++) 
            {
                Console.WriteLine($"{calendar[i]}");
            }

            Console.WriteLine(new string('-', 20));

            //List<string> list = calendar.MonthWithDays(10);
            List<string> list = calendar.MonthWithDays(30);

            if(list.Count == 0)
            {
                Console.WriteLine("Month that has this number of days can not be found");
            }
            else
            {
                foreach (string s in list)
                {
                    Console.WriteLine(s);
                }
            }
        }
    }
}