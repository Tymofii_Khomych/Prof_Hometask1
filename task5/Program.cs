﻿namespace task5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Использование Visual C# для реализации пользовательских коллекций
            // https://learn.microsoft.com/ru-ru/troubleshoot/developer/visualstudio/csharp/language-compilers/implement-custom-collection

            /*
             * Реализация интерфейса ICollection в пользовательском классе
             * Интерфейс ICollection наследуется от IEnumerable интерфейса. Интерфейс ICollection определяет CopyTo метод и три свойства, доступные 
             * только для чтения: IsSynchronized, SyncRootи Count. ICollection наследует GetEnumerator метод от IEnumerable интерфейса. 
             * Пользовательский класс коллекции должен реализовывать ICollection интерфейс.
               
            Чтобы реализовать ICollection интерфейс, выполните следующие действия.
                    -В Visual C# .NET создайте приложение Для Windows.
                    -В Обозреватель решений щелкните правой кнопкой мыши имя проекта, наведите указатель мыши на пункт Добавить, 
            а затем выберите Добавить класс, чтобы добавить модуль класса с именем CustomCollection.
                    -Добавьте следующий пример кода в начало модуля класса, чтобы импортировать System.Collection пространство имен using System.Collections;
                    -CopyTo Реализуйте метод , который принимает в качестве параметров целочисленный массив и индекс. Этот метод копирует элементы 
            из коллекции в массив, начиная с передаваемого индекса.
                    -GetEnumerator Реализуйте метод , который наследуется интерфейсом ICollection от IEnumerable. Метод GetEnumerator возвращает Enumerator 
            объект, который может выполнять итерацию по коллекции.
                    -
             */
        }
    }
}