﻿namespace task6
{
    class Program
    {
        static IEnumerable<int> GetOddNumberSquares(int[] numbers)
        {
            foreach (int number in numbers)
            {
                if (number % 2 != 0) 
                {
                    yield return number * number; 
                }
            }
        }

        static void Main(string[] args)
        {
            int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            IEnumerable<int> oddNumberSquares = GetOddNumberSquares(numbers);

            foreach (int square in oddNumberSquares)
            {
                Console.WriteLine(square);
            }
        }
    }
}