﻿namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Person john = new Person("John", 1980);
            Person mary = new Person("Mary", 1985);
            Person bob = new Person("Bob", 2000);
            Person alice = new Person("Alice", 2005);

            john.Children.Add(bob);
            john.Children.Add(alice);
            mary.Children.Add(bob);
            mary.Children.Add(alice);

            FamilyTree familyTree = new FamilyTree();
            familyTree.AddRelative(john);
            familyTree.AddRelative(mary);

            List<Person> descendants = familyTree.GetDescendants(john);
            Console.WriteLine("John's descendants:");
            foreach (var descendant in descendants)
            {
                Console.WriteLine(descendant.Name);
            }

            List<Person> relatives = familyTree.GetRelativesByBirthYear(2000);
            if (relatives.Count == 0)
            {
                Console.WriteLine("Any relatives born in that year");
            }
            else
            {
                Console.WriteLine("Relatives born in 2000:");
                foreach (var relative in relatives)
                {
                    Console.WriteLine(relative.Name);
                }
            }
        }
    }
}