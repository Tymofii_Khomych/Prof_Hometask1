﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Person
    {
        public string Name { get; set; }
        public int BirthYear { get; set; }
        public List<Person> Children { get; set; }

        public Person(string name, int birthYear)
        {
            Name = name;
            BirthYear = birthYear;
            Children = new List<Person>();
        }
    }
}
