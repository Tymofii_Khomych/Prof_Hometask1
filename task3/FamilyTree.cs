﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class FamilyTree
    {
        private List<Person> people;

        public FamilyTree()
        {
            people = new List<Person>();
        }

        public void AddRelative(Person person)
        {
            people.Add(person);
        }

        public void RemoveRelative(Person person)
        {
            people.Remove(person);
        }

        public List<Person> GetDescendants(Person person)
        {
            List<Person> descendants = new List<Person>();
            GetDescendantsRecursive(person, descendants);
            return descendants;
        }

        private void GetDescendantsRecursive(Person person, List<Person> descendants)
        {
            descendants.Add(person);
            foreach (var child in person.Children)
            {
                GetDescendantsRecursive(child, descendants);
            }
        }

        public List<Person> GetRelativesByBirthYear(int birthYear)
        {
            List<Person> relatives = new List<Person>();
            foreach (var person in people)
            {
                if (person.BirthYear == birthYear)
                {
                    relatives.Add(person);
                }
                relatives.AddRange(GetDescendantsByBirthYear(person, birthYear));
            }
            return relatives;
        }

        private List<Person> GetDescendantsByBirthYear(Person person, int birthYear)
        {
            List<Person> descendants = new List<Person>();
            foreach (var child in person.Children)
            {
                if (child.BirthYear == birthYear)
                {
                    descendants.Add(child);
                }
                descendants.AddRange(GetDescendantsByBirthYear(child, birthYear));
            }
            return descendants;
        }
    }
}
